<?php

namespace Drupal\commerce_promotion_views\Plugin\views\field;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("offer__target_plugin_configuration")
 */
class OfferTypeConfig extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;


  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrencyFormatterInterface $currency_formatter, ModuleHandlerInterface $module_handler, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currencyFormatter = $currency_formatter;
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_price.currency_formatter'),
      $container->get('module_handler'),
      $container->get('request_stack')
    );
  }

  /**
   * Define the available options.
   *
   * @return array
   *   An array of options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['unserialize'] = ['default' => 'FALSE'];
    $options['offer_config_display'] = ['default' => []];

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $form['unserialize'] = [
      '#title' => $this->t('Unserialize'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['unserialize'],
    ];

    $options = [
      "amount" => 'Amount',
      "percentage" => 'Percentage',
      "offer_percentage" => 'Offer Percentage',
    ];
    $form['offer_config_display'] = [
      '#title' => $this->t('Include in display'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->options['offer_config_display'],
      '#states' => [
        'visible' => [
          ':input[name="options[unserialize]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $value = $this->getValue($values);

    if ($this->options['unserialize'] == TRUE) {
      $data = [];
      $data = unserialize($value, $data);
      $output = [];

      // Display amount.
      if ($this->options['offer_config_display']['amount'] && (!empty($data['amount']) || !empty($data['offer_amount']))) {
        $price = NULL;

        // Fixed amount off.
        if (!empty($data['amount']['number'])) {
          $price = new Price((string) $data['amount']['number'], $data['amount']['currency_code']);
        }
        else {
          $price = new Price((string) $data['offer_amount']['number'], $data['offer_amount']['currency_code']);
        }
        if (!is_null($price)) {

          $formatted_price = $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode());
          $currency_cookie = $this->requestStack->getCurrentRequest()->cookies->get('commerce_currency');
          if ($this->moduleHandler->moduleExists('commerce_exchanger') && isset($currency_cookie)) {
            $convert_price = \Drupal::service('commerce_exchanger.calculate')
              ->priceConversion($price, $currency_cookie);
            $formatted_price = $this->currencyFormatter->format($convert_price->getNumber(), $convert_price->getCurrencyCode());
            $output['amount'] = [
              '#theme' => 'commerce_promotion_views',
              '#amount' => (string) $formatted_price,
            ];
          }
          else {
            $output['amount'] = [
              '#theme' => 'commerce_promotion_views',
              '#amount' => (string) $formatted_price,
            ];
          }
        }

      }

      // Display percentage.
      if (!empty($this->options['offer_config_display']['percentage']) && !empty($data['percentage'])) {
        $percentage = $data['percentage'] * 100;
        $output['percentage'] = [
          '#theme' => 'commerce_promotion_views',
          '#percentage' => $percentage,
        ];
      }
      if (!empty($this->options['offer_config_display']['offer_percentage']) && !empty($data['offer_percentage'])) {
        $percentage = $data['offer_percentage'] * 100;
        $output['offer_percentage'] = [
          '#theme' => 'commerce_promotion_views',
          '#percentage' => $percentage,
        ];
      }
      return $output;
    }

    return $this->sanitizeValue($value);

  }

}
