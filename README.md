CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/commerce_promotion_views

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/commerce_promotion_views


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

* Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

Install the EditionGuard API module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its
       dependencies.

USAGE
-------------
    1. Navigate to Administration > Commerce > Promotions
    2. Add a promotion or if you already have one then check "Enable Product & Product Variation reference" (disabled by default)
    3. In a product view or product variation view you can add a relation to the promotion
    4. In a view with promotion relation add the "Promotion: Offer type (target_plugin_configuration)" field.
    5. Check unserialize and choose what you want to display (e.g Amount, Percentage).
    6. You can now show promotions and their respective discounts on product pages using views.

MAINTAINERS
-----------

 * lexsoft - https://www.drupal.org/u/lexsoft
